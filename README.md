Mail-Sender
============

Ce programme envoie des mails périodiques composés de différentes sources (flux RSS, météo de forecast.io, pages web extraites du sitemap d'un site web, etc.)

**This is a work in progress**


Installation
============

- Télécharger et installer les sources : `git clone git@git.framasoft.org:cvfe/mail-sender.git`
- entrer dans le répertoire : `cd mail-sender`
- copier et mettre à jour le fichier de configuration forecast.io : `cp module/config_forecast.ini.dist /somewhere/else/config_forecast.ini`
- 


Usage
=====

Actuellement, seul le module de connection à forecast.io est en cours de développement.

```
python3 main.py /somewhere/else/config_forecast.ini
```

