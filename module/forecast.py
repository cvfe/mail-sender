import configparser
import logging
import configparser


def render(config_file):
   logger = logging.getLogger(__name__)
   logger.debug("Cool, it's working")
   logger.debug("this is a debug message")
   
   config_parser = configparser.ConfigParser()
   config_parser.read(config_file)
   logger.debug(config_parser.sections())
   config = config_parser['DEFAULT']
   logger.debug("The api key imported from config is : %s", config['api_key'])


   # your code here

   return "this is the forecast module string"
