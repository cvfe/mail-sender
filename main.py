import argparse
from module import forecast
import pathlib
import logging

logging.basicConfig(level=logging.DEBUG)

if __name__ == '__main__':
   parser = argparse.ArgumentParser(description='Send mails periodically.',
               epilog="""
MailSender  Copyright (C) 2016  CVFE a.s.b.l. \
This program comes with ABSOLUTELY NO WARRANTY; \
This is free software, and you are welcome to redistribute it \
under certain conditions; see LICENSE file for details 
""")
   parser.add_argument('config_forecast', type=str,
                      help='The path to the config file for forecast io')

   args = parser.parse_args()
   logger = logging.getLogger(__name__)
   
   # check config path exists
   p = pathlib.Path(args.config_forecast)
   if p.is_file():
      # we run the forecast module
      string = forecast.render(args.config_forecast)
   else:
      print("the file " + args.config_forecast + " is not found.")
      
   logger.debug("the module forecast.io is finished. I print here the result")
   print(string)

